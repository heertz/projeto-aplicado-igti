import Vue from 'vue'
import Router from 'vue-router'
import Auth from '@/services/Auth'
import DefaultLayout from '@/components/layouts/Default'

Vue.use(Router)

export default new Router({
  base: '/admin/',
  mode: 'history',
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: () => import('@/components/Login')
    },
    {
      path: '/',
      component: DefaultLayout,
      beforeEnter: requireAuth,
      children: [
        { path: '', component: () => import('@/components/Dashboard') },
        { path: 'perguntas', component: () => import('@/components/perguntas/List') }
      ]
    }
  ]
})

function requireAuth (to, from, next) {
  if (!Auth.loggedIn()) {
    next({ path: '/login' })
  } else {
    next()
  }
}
