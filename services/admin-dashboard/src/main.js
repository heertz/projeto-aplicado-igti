import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'

import bModal from 'bootstrap-vue/es/components/modal/modal'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

Vue.component('b-modal', bModal)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
