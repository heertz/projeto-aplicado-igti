export default {
  login (email, pass) {
    if (this.getToken()) {
      return Promise.resolve(true)
    }

    // @TODO: Implementar autenticação via API
    localStorage.token = Math.random().toString(36).substring(7)
    return Promise.resolve(true)
  },

  getToken () {
    return localStorage.token
  },

  logout () {
    delete localStorage.token
  },

  loggedIn () {
    return !!localStorage.token
  }
}
