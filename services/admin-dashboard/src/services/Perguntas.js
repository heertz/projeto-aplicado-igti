import Api from '@/services/Api';

export default {
    get () {
      return Api.get('perguntas').then(res => res.data);
    },

    salvar(pergunta = {}) {      
        return Api.post('perguntas', pergunta);
    },

    editar(pergunta = {}) {
        return Api.put(`perguntas/${pergunta.id}`, pergunta);
    },

    excluir(pergunta = {}) {
      return Api.delete(`perguntas/${pergunta.id}`);
    },

    atualizarOrdem(perguntas = []) {
      return Api.put('perguntas', perguntas);
    }
  }
  