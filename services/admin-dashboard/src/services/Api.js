import axios from 'axios';

let axiosInstance = axios.create({
    baseURL: 'https://localhost/api/'
});

export default axiosInstance;