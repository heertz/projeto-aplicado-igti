module.exports = {
    baseUrl: '/admin/',
    devServer: {
        port: 80,
        public: '0.0.0.0'
    }
}