var numPaginaInicialQuestoes = 3;
var numPaginaFinalQuestoes = null;
/**
 * Retorna o número da página em que uma determinada questão se encontra
 * @param {*} qt 
 */
function getQtPageNumber(qt) {
    return numPaginaInicialQuestoes + (qt - 1);
}

/**
 * Retorna o número da questão que se encontra em determinada página
 * @param {*} page 
 */
function getPageNumberQt(page) {
    return page - (numPaginaInicialQuestoes - 1);
}

var startPagination = function() {
    var _this = this;
    _this.$pages = $('#pt-main').find('.pt-page');
    _this.current = 0;
    _this.$currPage = _this.$pages.eq(_this.current);
    _this.$nextPage = null;
    _this.isAnimating = false;
    _this.tipoDeAtendimento = null;
    _this.isAnonimo = false;
    _this.onAnimationEndHandler = function() { _this.onAnimationEnd() };

    numPaginaFinalQuestoes = _this.$pages.length;

    var animEndEventNames = {
        'WebkitAnimation' : 'webkitAnimationEnd',
        'OAnimation' : 'oAnimationEnd',
        'msAnimation' : 'MSAnimationEnd',
        'animation' : 'animationend'
    };

    // animation end event name
    _this.animEndEventName = animEndEventNames[ Modernizr.prefixed('animation')];

    // Reseta os valores de todas as questões
    $('input[name^=qt]:checked').prop('checked', false);

    $('.nextPage').click(function(e) { _this.nextPage(e) });
    $('.prevPage').click(function(e) { _this.prevPage(e) });
    $('.btnRevisaoQts button').click(function(e) {
        var numQuestao = $(e.target).data('qt');
        _this.prevPage(e, getQtPageNumber(numQuestao));
    });
    _this.$pages.each(function() {
        var $this = $(this);
        $this.data('originalClassList', $this.attr('class'));
    });

    _this.$currPage.addClass('pt-page-current');

    _this.nextPage = function(event, next) {
        next = (next !== undefined ? next : _this.current + 1);

        // Caso o usuário informe a satisfação, é obrigatória informar a expectativa e vice-versa
        if (_this.current >= numPaginaInicialQuestoes && _this.current <= numPaginaFinalQuestoes) {
            var numQt = getPageNumberQt(_this.current).toString().padStart(2, 0),
                isExpectativaChecked = $('input[name=qt'+ numQt +'aExp]:checked').length !== 0,
                isSatisfacaoChecked = $('input[name=qt'+ numQt +'aSat]:checked').length !== 0;

            if (isSatisfacaoChecked && !isExpectativaChecked) {
                alert('Informe a sua expectativa!');
                return;
            }

            if (isExpectativaChecked && !isSatisfacaoChecked) {
                alert('Informe a sua satisfação!');
                return;
            }
        }

        var $evTarget = $(event.target);
        var animation = 5;

        // Validações específicas de algumas telas
        switch(next) {
            case 1: //Dados básicos
                ajaxGet('dados-basicos', function(dados) {
                    // Adiciona as opções de "sexo"
                    var sexoSelect = $('#sexo'),
                        sexoHTML   = '';
                    
                    sexoSelect.empty();
                    $.each(dados.sexos, function(i, sexo) {
                        sexoHTML += '<option value="'+ sexo.id_sexo +'">'+ sexo.ds_sexo +'</option>';
                    });
                    sexoSelect.append(sexoHTML);

                    // Adiciona as opções de "escolaridade"
                    var escolaridadeSelect = $('#grau-escolaridade'),
                        escolaridadeHTML   = '';
                    
                    escolaridadeSelect.empty();
                    $.each(dados.escolaridades, function(i, escolaridade) {
                        escolaridadeHTML += '<option value="'+ escolaridade.id_tipo_escolaridade +'">'+ escolaridade.ds_tipo_escolaridade +'</option>';
                    });
                    escolaridadeSelect.append(escolaridadeHTML);
                });
                break;

            case 2: //Dados da instituição
                _this.isAnonimo = ($evTarget.data('anonimo') === 1);

                ajaxGet('estados', function(estados) {
                    // Adiciona as opções de "UF"
                    var ufSelect = $('#uf'),
                        ufHTML   = '';
                        
                    ufSelect.empty();
                    $.each(estados, function(i, uf) {
                        ufHTML += '<option value="'+ uf.id +'">'+ uf.nome +'</option>';
                    });
                    ufSelect.append(ufHTML);
                    ufSelect.change();
                });

                break;

            case (numPaginaFinalQuestoes - 2): // Tela de revisão de notas
                // Pega os valores das notas para revisão
                for (var qt = 1; qt <= 12; qt++) {
                    if (qt === 6 || qt === 7) {
                        if ($.inArray(_this.tipoDeAtendimento, [4, 6]) !== -1) {
                            $('.btnRevisaoQts div').eq(qt - 1).hide();
                            continue;
                        }
                        $('.btnRevisaoQts div').eq(qt - 1).show();
                    } else if (qt === 8) {
                        if ($.inArray(_this.tipoDeAtendimento, [4, 6, 7]) === -1) {
                            $('.btnRevisaoQts div').eq(qt - 1).hide();
                            continue;
                        }
                        $('.btnRevisaoQts div').eq(qt - 1).show();
                    } else if (qt === 9) {
                        if ($.inArray(_this.tipoDeAtendimento, [1, 2]) !== -1) {
                            $('.btnRevisaoQts div').eq(qt - 1).hide();
                            continue;
                        }
                        $('.btnRevisaoQts div').eq(qt - 1).show();
                    }

                    var numQt = qt.toString().padStart(2, '0');
                    $('.notaExpRev').eq(qt - 1).text($('input[name=qt'+ numQt +'aExp]:checked').val());
                    $('.notaSatRev').eq(qt - 1).text($('input[name=qt'+ numQt +'aSat]:checked').val())
                }
                break;

            case (numPaginaFinalQuestoes - 1): // Tela de finalização 1
                _this.sendAnswers(function(data) {
                    if (!data) return;

                    $('#num_protocolo').text(data.protocolo);
                });
                break;

            case numPaginaFinalQuestoes: // Tela de finalização 2
                //@TODO: Apagar os dados do formulários
                _this.nextPage(event, 0);
                return;
        }

        _this.animatePage(_this.current, next, animation);
    };

    this.prevPage = function(event, prev) {
        prev = (prev !== undefined ? prev : _this.current - 1);

        if (prev < 0) return;

        var animation = 6;

        switch (prev) {
            case 10:
                switch (_this.tipoDeAtendimento) {
                    case 4: // Setor de exames
                    case 6: // Setor de farmácia voltam para questão 05
                        _this.prevPage(event, getQtPageNumber(5));
                        return;
                }
                break;

            case 11:
                switch (this.tipoDeAtendimento) {
                    case 3: // Dentista
                    case 5: // Fisioterapia volta para questão 07
                        _this.prevPage(event, getQtPageNumber(7));
                        return;
                }
                break;
            case 12:
                switch (this.tipoDeAtendimento) {
                    case 1: // Médico
                    case 2: // Enfermagem volta para questão 07
                        _this.prevPage(event, getQtPageNumber(7));
                        return;
                }
                break;
        }

        _this.animatePage(_this.current, prev, animation);
    };

    this.animatePage = function(currentPage, nextPage, animation) {
        if (_this.isAnimating) return false;

        _this.isAnimating = true;
        _this.$currPage = this.$pages.eq(currentPage);
        _this.$nextPage = this.$pages.eq(nextPage);

        // Volta a página para o topo, útil principalmente em smartphones
        _this.$currPage.scrollTop(0);

        var outClass = '', inClass = '';
        switch (parseInt(animation)) {
            case 1:
                outClass = 'pt-page-moveToLeftEasing pt-page-ontop';
                inClass = 'pt-page-moveFromRight';
                break;
            case 2:
                outClass = 'pt-page-moveToRightEasing pt-page-ontop';
                inClass = 'pt-page-moveFromLeft';
                break;
            case 5:
                outClass = 'pt-page-fade';
                inClass = 'pt-page-moveFromRight pt-page-ontop';
                break;
            case 6:
                outClass = 'pt-page-fade';
                inClass = 'pt-page-moveFromLeft pt-page-ontop';
                break;
        }

        _this.$currPage.addClass(outClass);

        _this.$nextPage.on(_this.animEndEventName, _this.onAnimationEndHandler);
        _this.$nextPage.addClass(inClass +' pt-page-current');

        _this.current = nextPage;

        // Barra de progresso;
        var totalPages = _this.current <= 16 ? 16 : 37,
            progress = (_this.current * 100) / totalPages;
        $('#barra-progresso').width(progress+'%');
    };

    this.onAnimationEnd = function() {
        _this.$nextPage.off(_this.animEndEventName);
        _this.$currPage.attr('class', _this.$currPage.data('originalClassList'));
        _this.$nextPage.attr('class', _this.$nextPage.data('originalClassList') + ' pt-page-current');
        _this.isAnimating = false;
    };

    this.sendAnswers = function(callback) {
        var sData = _this.serializeAnswers();
        ajaxPost('respostas', sData, function(data) {
            if (!data) throw new Error('Aborted');

            if (data.error === true) throw new Error(data.message);

            callback(data);
        });
    };

    this.serializeAnswers = function() {
        var inputs = $('input[type=text], input[type=email], select, input[type=radio]:checked'),
            serializedData = {};

        inputs.each(function (i, input) {
            serializedData[input.name || input.id] = $(input).val();
        });

        if (_this.isAnonimo) serializedData.isAnonimo = true;

        return serializedData;
    }
};