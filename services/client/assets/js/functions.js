/**
 * Parâmatros de configuração base para AJAX
 * @type {string}
 */
var baseURL = 'https://localhost/api/';
var ajaxDefaultParams = {
    headers: {'Accept': 'application/json'}
};

/**
 * Realiza uma requisão GET para a URL informada
 * @param url
 * @param callback
 */
function ajaxGet(url, callback) {
    var options = $.extend({
        url     : baseURL + url,
        success : callback
    }, ajaxDefaultParams);
    $.get(options);
};

/**
 * Realiza uma requisão POST para a URL informada
 * @param url
 * @param callback
 */
function ajaxPost(url, data, callback) {
    var options = $.extend({
        url     : baseURL + url,
        data    : data,
        success : callback
    }, ajaxDefaultParams);
    $.post(options);
};

$(function(event) {
    // Máscaras de telefone
    var maskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    };

    $('#tel-1, #tel-2').mask(maskBehavior, {
        onKeyPress: function(val, e, field, options) {
            field.mask(maskBehavior.apply({}, arguments), options);
        }
    });

    buscaPerguntas();
});

var filtrosBuscaInstituicoes = {
    uf               : null,
    municipio        : null,
    bairo            : null,
    tipo_instituicao : null
};
function buscaInstituicoes() {
    // Remove todos os valores "null" para não enviar na requisão
    notNullFilters = $.extend({}, filtrosBuscaInstituicoes);
    $.each(notNullFilters, function(i, filter) { if (!filter) delete notNullFilters[i]; });

    ajaxGet('instituicoes?'+ $.param(notNullFilters), function(instituicoes) {
        // Adiciona as opções de "Município"
        var instituicaoSelect = $('#instituicao'),
            instituicaoHTML   = '';

        instituicaoSelect.empty();
        $.each(instituicoes, function(i, instituicao) {
            instituicaoHTML += '<option value="'+ instituicao.id +'">'+ instituicao.nome +'</option>';
        });
        instituicaoSelect.append(instituicaoHTML);
        instituicaoSelect.change();
    });
}

function buscaPerguntas() {
    ajaxGet('perguntas', (perguntas) => {
        renderizarPerguntas(perguntas);
        startPagination();
    });
}

function renderizarPerguntas(perguntas) {
    let html = '';
    perguntas.forEach((pergunta, index) => {
        let ordem = pergunta.ordem.toString().padStart(2, 0);;
        html += `<div class="pt-page">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="left_form">
                                <figure><img src="assets/img/registration_bg.svg" alt=""></figure>
                                <h2>Questão ${ordem}</h2>
                            </div>
                        </div>
                        <div class="content col-md-8">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <h3 class="main_question">${pergunta.pergunta}</h3>
                                        <h4>Qual era sua expectativa?</h4>
                                        <div class="row text-center">
                                            <div class="col-2 offset-1">
                                                <label class="niveis-satisfacao muito-insatisfeito">
                                                    <input type="radio" name="qt${ordem}aExp" value="1">
                                                </label><br>
                                                Ruim
                                            </div>
                                            <div class="col-2">
                                                <label class="niveis-satisfacao insatisfeito">
                                                    <input type="radio" name="qt${ordem}aExp" value="2">
                                                </label><br>
                                                Regular
                                            </div>
                                            <div class="col-2">
                                                <label class="niveis-satisfacao neutro">
                                                    <input type="radio" name="qt${ordem}aExp" value="3">
                                                </label><br>
                                                Bom
                                            </div>
                                            <div class="col-2">
                                                <label class="niveis-satisfacao satisfeito">
                                                    <input type="radio" name="qt${ordem}aExp" value="4">
                                                </label><br>
                                                Ótimo
                                            </div>
                                            <div class="col-2">
                                                <label class="niveis-satisfacao muito-satisfeito">
                                                    <input type="radio" name="qt${ordem}aExp" value="5">
                                                </label><br>
                                                Excelente
                                            </div>
                                        </div>
                                        <br>
                                        <h4>Qual a sua satisfação?</h4>
                                        <div class="row text-center">
                                            <div class="col-2 offset-1">
                                                <label class="niveis-satisfacao muito-insatisfeito">
                                                    <input type="radio" name="qt${ordem}aSat" value="1">
                                                </label><br>
                                                Ruim
                                            </div>
                                            <div class="col-2">
                                                <label class="niveis-satisfacao insatisfeito">
                                                    <input type="radio" name="qt${ordem}aSat" value="2">
                                                </label><br>
                                                Regular
                                            </div>
                                            <div class="col-2">
                                                <label class="niveis-satisfacao neutro">
                                                    <input type="radio" name="qt${ordem}aSat" value="3">
                                                </label><br>
                                                Bom
                                            </div>
                                            <div class="col-2">
                                                <label class="niveis-satisfacao satisfeito">
                                                    <input type="radio" name="qt${ordem}aSat" value="4">
                                                </label><br>
                                                Ótimo
                                            </div>
                                            <div class="col-2">
                                                <label class="niveis-satisfacao muito-satisfeito">
                                                    <input type="radio" name="qt${ordem}aSat" value="5">
                                                </label><br>
                                                Excelente
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 text-center">
                                <div class="btn-group">
                                    <button type="button" name="backward" class="btn btn-danger prevPage">Voltar</button>
                                    <button type="button" class="btn btn-success nextPage">Continuar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`;
    });
    $('#dados-instituicao').after(html);
}

// Altera a classe dos inputs de "Tipo de atendimento"
$('input[name=tipo-atendimento]').click(function(e) {
    $('#selecao-tipo-atendimento').find('label.active').removeClass('active');
    $(this).parent().addClass('active');
    $('#profissionais').prop('multiple', (e.target.value == 7));

    //@TODO: Verificar scroll to bottom
    // transitions.$nextPage.scrollTop = transitions.$nextPage.scrollHeight;
});

// Altera a classe dos inputs dos níveis de satisfação
$('#pt-main').on('click', '.niveis-satisfacao', function() {
    var $this = $(this);
    $this.closest('div.row').find('.radio-checked').removeClass('radio-checked');
    $this.addClass('radio-checked');
    $this.find('input').prop('checked', true);
});

$('#uf').change(function() {
    // Ao alterar a uf, é necessário remover os filtros de menor hierarquia
    filtrosBuscaInstituicoes.municipio = null;
    filtrosBuscaInstituicoes.bairo = null;
    filtrosBuscaInstituicoes.tipo_instituicao = null;

    var selectedUf = $(this).val();
    if (!selectedUf) return;

    filtrosBuscaInstituicoes.uf = selectedUf;

    ajaxGet('estados/'+ selectedUf +'/municipios', function(municipios) {
        // Adiciona as opções de "Município"
        var municipioSelect = $('#municipio'),
            municipiosHTML  = '';

        municipioSelect.find('option:not(:first)').remove();
        $.each(municipios, function(i, municipio) {
            municipiosHTML += '<option value="'+ municipio.id +'">'+ municipio.nome +'</option>';
        });
        municipioSelect.append(municipiosHTML);
    });

    buscaInstituicoes();
});

$('#municipio').change(function() {
    // Ao alterar o munícipio, é necessário remover os filtros de menor hierarquia
    filtrosBuscaInstituicoes.bairo = null;
    filtrosBuscaInstituicoes.tipo_instituicao = null;

    var selectedMunicipio = $(this).val();
    if (!selectedMunicipio) {
        filtrosBuscaInstituicoes.municipio = null;
        return;
    } else {
        filtrosBuscaInstituicoes.municipio = selectedMunicipio;
    }

    ajaxGet('municipios/'+ selectedMunicipio +'/bairros', function(bairros) {
        // Adiciona as opções de "Bairro"
        var bairrosSelect = $('#bairro'),
            bairrosHTML   = '<option value="">Selecione o bairro</option>';

        bairrosSelect.empty();
        $.each(bairros, function(i, bairro) {
            bairrosHTML += '<option value="'+ bairro.nome +'">'+ bairro.nome +'</option>';
        });
        bairrosSelect.append(bairrosHTML);
    });

    getTiposInstituicoesMunicipio(selectedMunicipio);
    buscaInstituicoes();
});

$('#bairro').change(function() {
    // Ao alterar o barrio, é necessário remover o filtro de menor hierarquia
    filtrosBuscaInstituicoes.tipo_instituicao = null;

    var selectedMunicipio = $('#municipio').val(),
        selectedBairro = $(this).val();
    if (!selectedBairro) {
        filtrosBuscaInstituicoes.bairro = null;
        getTiposInstituicoesMunicipio(selectedMunicipio);
    } else {
        filtrosBuscaInstituicoes.bairro = selectedBairro;

        ajaxGet('municipios/'+ selectedMunicipio +'/bairro/'+ selectedBairro +'/tipos-instituicoes', function(tiposUnidades) {
            // Adiciona as opções de "Tipo da instituição"
            var tiposUnidadesSelect = $('#tipo-instituicao'),
                tiposUnidadesHTML   = '<option value="">Selecione o tipo de instiuição</option>';
    
            tiposUnidadesSelect.empty();
            $.each(tiposUnidades, function(i, tipoUnidade) {
                tiposUnidadesHTML += '<option value="'+ tipoUnidade.id +'">'+ tipoUnidade.tipo_unidade +'</option>';
            });
            tiposUnidadesSelect.append(tiposUnidadesHTML);
        });
    }

    buscaInstituicoes();
});

$('#tipo-instituicao').change(function() {
    var selectedTipo = $(this).val();

    if (!selectedTipo) {
        filtrosBuscaInstituicoes.tipo_instituicao = null;
        buscaInstituicoes(); return;
    } else {
        filtrosBuscaInstituicoes.tipo_instituicao = selectedTipo;
    }

    buscaInstituicoes();
});

$('#instituicao').change(function() {
    var selectedInstituicao = $(this).val(),
        profissionalSelect  = $('#profissionais-group');

    if (!selectedInstituicao) {
        profissionalSelect.empty();
        profissionalSelect.append($('<option>', {value: null, text: 'Sem profissional cadastrado para a unidade selecionada.'}));
        return;
    }
});

function getTiposInstituicoesMunicipio(municipioId) {
    ajaxGet(`municipios/${municipioId}/tipos-instituicoes`, function(tiposUnidades) {
        // Adiciona as opções de "Tipo da instituição"
        var tiposUnidadesSelect = $('#tipo-instituicao'),
            tiposUnidadesHTML   = '<option value="">Selecione o tipo de instiuição</option>';

        tiposUnidadesSelect.empty();
        $.each(tiposUnidades, function(i, tipoUnidade) {
            tiposUnidadesHTML += '<option value="'+ tipoUnidade.id +'">'+ tipoUnidade.tipo_unidade +'</option>';
        });
        tiposUnidadesSelect.append(tiposUnidadesHTML);
    });
}