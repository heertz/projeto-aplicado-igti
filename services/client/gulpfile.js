const gulp     = require('gulp');
const concat   = require('gulp-concat');
const uglify   = require('gulp-uglify/composer')(require('uglify-es'), console);
const cleanCSS = require('gulp-clean-css');

gulp.task('generate-service-worker', function(callback) {
    var swPrecache = require('sw-precache');
    var rootDir = '';

    swPrecache.write('service-worker.js', {
        runtimeCaching: [{
            urlPattern: /^https:\/\/localhost\/api/,
            handler: 'networkFirst'
        }],
        staticFileGlobs: [
            'index.html',
            'assets/img/*.{svg,png}',
            'assets/fonts/*.{woff,woff2}',
            'dist/vendor.min.css',
            'dist/app.min.css',
            'dist/vendor.min.js',
            'dist/app.min.js'
        ],
        stripPrefix: rootDir
    }, callback);
});

gulp.task('app-js', function() {
    return gulp.src([
                   'assets/js/polyfills.js',
                   'assets/js/modernizr-custom.js',
                   'assets/js/pagetransitions.js',
                   'assets/js/functions.js',
                   'service-worker-registration.js',
               ])
               .pipe(concat('app.min.js'))
               .pipe(uglify())
               .pipe(gulp.dest('./dist/'));
});

gulp.task('jquery', gulp.series(
    function clearClone() {
        var del = require('del');

        return del('./jquery/');
    },

    function clonejQuery() {
        var git = require('gulp-git');

        return git.clone('https://github.com/jquery/jquery.git', {args: '--single-branch -b 3.3.1'}, function(err) { console.log(err); })
    },

    function buildCustom() {
        var run = require('gulp-run');

        return run('cd jquery && npm run build -- custom:-deprecated,-effects,-exports/amd,-wrap').exec();
    }
));

gulp.task('vendor-js', gulp.series('jquery', function() {
    return gulp.src([
                   './jquery/dist/jquery.min.js',
                   'node_modules/jquery-mask-plugin/dist/jquery.mask.min.js'
               ])
               .pipe(concat('vendor.min.js'))
               .pipe(uglify())
               .pipe(gulp.dest('./dist/'));
}));

gulp.task('app-css', function() {
    return gulp.src([
                   'assets/css/animations.css',
                   'assets/css/style.css',
                   'assets/css/responsive.css',
                   'assets/css/fonts.css'
               ])
               .pipe(concat('app.min.css'))
               .pipe(cleanCSS({ rebaseTo : './dist/' }))
               .pipe(gulp.dest('./dist/'));
});

gulp.task('bootstrap', function() {
    var sass = require('gulp-sass'),
        rename = require('gulp-rename');
    return gulp.src('./assets/scss/bootstrap-custom.scss')
               .pipe(sass())
               .pipe(rename('bootstrap.css'))
               .pipe(gulp.dest('./assets/css/'));
});

gulp.task('vendor-css', gulp.series('bootstrap', function() {
    return gulp.src([
                   'assets/css/bootstrap.css' // Para gerar o css do bootstrap, é preciso utilizar o SASS e compilar o arquivo bootstrap-custom.scss
               ])
               .pipe(concat('vendor.min.css'))
               .pipe(cleanCSS())
               .pipe(gulp.dest('./dist/'));
}));

gulp.task('minify-js', gulp.series('vendor-js', 'app-js'));
gulp.task('minify-css', gulp.series('vendor-css', 'app-css'));
gulp.task('minify-all', gulp.parallel('minify-js', 'minify-css'));
gulp.task('generate-assets', gulp.series('minify-all', 'generate-service-worker'));