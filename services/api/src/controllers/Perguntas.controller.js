const express = require('express'),
      models  = require('../models');

let router = express.Router();

router.get('/', (req, res) => {
    models.Pergunta.findAll({
        order: ['ordem']
    }).then(perguntas => {
        res.json(perguntas);
    });
});

router.post('/', (req, res) => {
    let data = req.body;

    models.sequelize.transaction(t => {
        return models.Pergunta.create({
            pergunta: data.pergunta,
            ordem: data.ordem
        }, { transaction : t })
        .catch(err => {
            throw new Error('Erro ao salvar a pergunta');
        })
    }).then(() => {
        res.json({ sucess: true });
    }).catch(err => {
        res.json({ error: true, message: err.toString().substr(7) });
    });
});

router.put('/', (req, res) => {
    let data = req.body,
        promises = [];

    models.sequelize.transaction(t => {
        data.forEach(pergunta => {
            promises.push(
                models.Pergunta.update(
                    { ordem: pergunta.ordem },
                    { where: { id: pergunta.id }, transaction : t }
                )
            );
        });

        return Promise.all(promises);
    }).then(() => {
        res.json({ sucess: true });
    }).catch(err => {
        res.json({ error: true, message: err.toString().substr(7) });
    });
});

router.put('/:perguntaId', (req, res) => {
    let data = req.body,
        promises = [];

    models.sequelize.transaction(t => {
        return models.Pergunta.update(
            { pergunta: data.pergunta },
            { where: { id: data.id }, transaction : t }
        )
    }).then(() => {
        res.json({ sucess: true });
    }).catch(err => {
        res.json({ error: true, message: err.toString().substr(7) });
    });
});

router.delete('/:perguntaId', (req, res) => {
    let perguntaId = req.params.perguntaId;

    models.sequelize.transaction(t => {
        return models.Pergunta.destroy(
            { where: { id: perguntaId }, transaction : t }
        )
    }).then(() => {
        res.json({ sucess: true });
    }).catch(err => {
        res.json({ error: true, message: err.toString().substr(7) });
    });
});

module.exports = router;