const express = require('express'),
      models  = require('../models');

let router = express.Router();

router.get('/', (req, res) => {
    let InstituicaoModel = models.Instituicao;
    let allowedParams  = ['uf', 'municipio', 'bairro', 'tipo_instituicao'];
    let filteredParams = Object.keys(req.query)
                               .filter(key => allowedParams.includes(key))
                               .reduce((carry, param) => {
                                   carry[param] = req.query[param];
                                   return carry;
                               }, {});
    
    let filterMap = {
        'bairro'           : 'no_bairro',
        'municipio'        : 'id_municipio',
        'tipo_instituicao' : 'id_tipo_unidade'
    };
    
    let options = {
        attributes : ['id', 'nome'],
        where : {},
        order: ['nome']
    };
    for (let param in filteredParams) {
        let columnName  = filterMap[param],
            filterValue = filteredParams[param];

        if (param === 'uf') {
            //Apenas consulta a UF se o município não for informado
            if (filteredParams['municipio'] !== undefined) continue;
            
            options.include= [{
                model: models.Municipio,
                attributes: [],
                where: {'uf_id' : filterValue}
            }];
        } else {
            options.where[columnName] = models.sequelize.where(
                models.sequelize.fn('upper', models.sequelize.col(columnName)),
                filterValue.toUpperCase()
            );
        }
    }
    
    models.Instituicao.findAll(options).then(estados => res.json(estados));
});

router.get('/:instituicaoId/profissionais', (req, res) => {
    models.Instituicao.findById(req.params.instituicaoId, {
        attributes : [],
        include    : [{
            model    : models.Profissional,
            attributes : ['id', 'nome'],
            required : true
        }],
        order : [[models.Profissional, 'nome']]
    }).then(instituicaoProfissionais => {
        res.json(instituicaoProfissionais ? instituicaoProfissionais.Profissionais : []);
    });
});

module.exports = router;