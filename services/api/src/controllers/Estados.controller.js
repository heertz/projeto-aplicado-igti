const express = require('express'),
      models  = require('../models');

let router = express.Router();

router.get('/', (req, res) => {
    models.Estado.findAll({
        order: ['nome']
    }).then(estados => {
        res.json(estados);
    });
});

router.get('/:estadoId/municipios', (req, res) => {
    models.Estado.findById(req.params.estadoId, {
        include: [
            {model: models.Municipio, required: true, attributes: ['id', 'nome']}
        ],
        order: [[models.Municipio, 'nome']],
    }).then(estadoMunicipios => {
        res.json(estadoMunicipios.Municipios);
    });
});

module.exports = router;