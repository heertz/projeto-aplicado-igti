const express = require('express'),
      models  = require('../models'),
      moment  = require('moment'),
      router  = express.Router();

router.post('/', (req, res) => {
    let data = req.body;

    const TIPO_ATENDIMENTO_MEDICO       = 1;
    const TIPO_ATENDIMENTO_ENFERMAGEM   = 2;
    const TIPO_ATENDIMENTO_DENTISTA     = 3;
    const TIPO_ATENDIMENTO_EXAMES_LAB   = 4;
    const TIPO_ATENDIMENTO_FISIOTERAPIA = 5;
    const TIPO_ATENDIMENTO_FARMACIA     = 6;
    const TIPO_ATENDIMENTO_INTERNACAO   = 7;

    let anoMes    = moment().format('YYYYMM'),
        protocolo = anoMes + Math.floor(Math.random() * 99999).toString().padStart(5, 0);

    models.sequelize.transaction(t => {
        participanteId = null;
        if (!data.isAnonimo) {
            models.Participante.create({
                documento            : data['cpf-rg'],
                nome                 : data['nome'],
                sexo_id              : data['sexo'],
                tipo_escolaridade_id : data['grau-escolaridade'],
                ano_nascimento       : data['ano-nascimento'],
                email                : data['email'],
                tipo_participante_id : 'U',
                protocolo            : protocolo
            }, {transaction: t})
            .then(participante => {
                participanteId = participante.id;
            })
            .catch(err => {
                throw new Error('Não foi possível salvar o participante.');
            });
        }

        let questoesNotasMap = {};
        for (let key in data) {
            if (! (matches = key.match(/^qt(\d{2})(a|b)(Sat|Exp)$/))) continue;

            let value = data[key],
                num   = parseInt(matches[1]),
                etapa = matches[2] === 'a' ? 6 : 5,
                tipo  = matches[3] === 'Sat' ? 'satisfacao' : 'expectativa';

            let variante = 'A';
            switch (etapa) {
                case 6:
                    switch (num) {
                        case 6:
                        case 7:
                            switch (data['tipo-atendimento']) {
                                case TIPO_ATENDIMENTO_MEDICO:
                                    variante = 'A';
                                    break;
                                case TIPO_ATENDIMENTO_DENTISTA:
                                    variante = 'B';
                                    break;
                                case TIPO_ATENDIMENTO_ENFERMAGEM:
                                    variante = 'C';
                                    break;
                                case TIPO_ATENDIMENTO_FISIOTERAPIA:
                                    variante = 'D';
                                    break;
                                case TIPO_ATENDIMENTO_INTERNACAO:
                                    variante = 'E';
                                    break;
                                case TIPO_ATENDIMENTO_EXAMES_LAB:
                                    variante = 'F';
                                    break;
                            }
                            break;
                        case 8:
                            switch (data['tipo-atendimento']) {
                                case TIPO_ATENDIMENTO_FARMACIA:
                                    variante = 'A';
                                    break;
                                case TIPO_ATENDIMENTO_EXAMES_LAB:
                                    variante = 'B';
                                    break;
                                case TIPO_ATENDIMENTO_INTERNACAO:
                                    variante = 'C';
                                    break;
                            }
                            break;
                        case 9:
                            //@TODO: Implementar após verificação se a questão 09 terá variante
                            break;
                    }
                    break;
            }

            let questaoId = `${num}${variante}${etapa}`;
            if (!questoesNotasMap.hasOwnProperty(questaoId)) {
                questoesNotasMap[questaoId] = { etapa : etapa}
            }
            questoesNotasMap[questaoId][tipo] = value;
        }

        let promises = [];
        for (let questaoId in questoesNotasMap) {
            let nota = questoesNotasMap[questaoId];

            promises.push(
                models.Nota.create({
                    instituicao_id   : data['instituicao'],
                    etapa_id         : nota['etapa'],
                    questao_id       : questaoId,
                    nota_expectativa : nota['expectativa'],
                    nota_satisfacao  : nota['satisfacao'],
                    participante_id  : participanteId,
                }, { transaction : t })
                .catch(err => {
                    throw new Error('Erro ao salvar a nota');
                })
            );
        }

        return Promise.all(promises);
    }).then(() => {
        res.json({ protocolo: protocolo });
    }).catch(err => {
        res.json({ error: true, message: err.toString().substr(7) });
    });
});

module.exports = router;