const express = require('express'),
      router  = express.Router(),
      models  = require('../models')
      Op      = models.sequelize.Op;

router.get('/', (req, res) => {
    models.Municipio.findAll({order: ['nome']}).then(municipios => res.json(municipios));
});

router.get('/:municipioId/bairros', (req, res) => {
    models.Municipio.findById(req.params.municipioId, {
        attributes: [],
        include: [{
            model: models.Instituicao,
            attributes: ['bairro'],
            required: true
        }],
        order: [[models.Instituicao, 'bairro']],
        group: [[models.Instituicao, 'bairro']]
    }).then(municipioInstituicoes => {
        let bairros = municipioInstituicoes.toJSON().Instituicoes
                                           .map(instituicao => new Object({nome: instituicao.bairro}));
        res.json(bairros);
    });
});

router.get('/:municipioId/bairro/:bairro/tipos-instituicoes', (req, res) => {
    models.Municipio.findById(req.params.municipioId, {
        attributes: [],
        include: [{
            model: models.Instituicao,
            attributes: ['id_tipo_unidade'],
            where: { bairro: {[Op.like] : req.params.bairro} },
            include: {
                model: models.InstituicaoTipo,
                required: true
            }
        }],
        group: models.sequelize.col('Instituicoes->InstituicaoTipo.ds_tipo_unidade')
    }).then(bairroInstituicoes => {
        let tiposInstituicoes = bairroInstituicoes.toJSON().Instituicoes
                                                  .map(instituicao => new Object({
                                                      id: instituicao.InstituicaoTipo.id,
                                                      tipo_unidade: instituicao.InstituicaoTipo.nome
                                                  }));
        res.json(tiposInstituicoes);
    });
});

router.get('/:municipioId/tipos-instituicoes', (req, res) => {
    models.Municipio.findById(req.params.municipioId, {
        attributes: [],
        include: [{
            model: models.Instituicao,
            attributes: ['id_tipo_unidade'],
            required: true,
            include: {
                model: models.InstituicaoTipo,
                required: true
            }
        }],
        group: models.sequelize.col('Instituicoes->InstituicaoTipo.ds_tipo_unidade'),
    }).then(municipioInstituicoes => {
        let tiposInstituicoes = municipioInstituicoes.toJSON().Instituicoes
                                                     .map(instituicao => new Object({
                                                         id: instituicao.InstituicaoTipo.id,
                                                         tipo_unidade: instituicao.InstituicaoTipo.nome
                                                     }));
        res.json(tiposInstituicoes);
    });
});

module.exports = router;