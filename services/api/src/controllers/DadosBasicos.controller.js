const express = require('express'),
      models  = require('../models');

let router = express.Router();

router.get('/', (req, res) => {
    Promise.all([models.Sexo.findAll(), models.Escolaridade.findAll()]).then(values => {
        res.json({
            'sexos'         : values[0],
            'escolaridades' : values[1]
        });
    });
});

module.exports = router;