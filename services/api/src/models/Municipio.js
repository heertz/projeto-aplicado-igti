module.exports = (sequelize, DataTypes) => {
    var Municipio = sequelize.define('Municipio', {
        id  : { type: DataTypes.INTEGER, primaryKey: true, field: 'id_municipio' },
        nome: { type: DataTypes.STRING, field: 'no_mun_completo' },
        uf_id : { type: DataTypes.INTEGER, field: 'id_uf' }
    }, {
        tableName: 'tb_municipio',
        timestamps: false
    });

    Municipio.associate = models => {
        models.Municipio.belongsTo(models.Estado, {foreignKey: 'id_uf'});
        models.Municipio.hasMany(models.Instituicao, {foreignKey: 'id_municipio'});
    };

    return Municipio;
};