module.exports = (sequelize, DataTypes) => {
    var Sexo = sequelize.define('Sexo', {
        id_sexo  : { type: DataTypes.INTEGER, primaryKey: true },
        ds_sexo: DataTypes.STRING
    }, {
        tableName: 'td_sexo',
        timestamps: false
    });

    return Sexo;
};