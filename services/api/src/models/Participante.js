module.exports = (sequelize, DataTypes) => {
    var Participante = sequelize.define('Participante', {
        id                   : { type: DataTypes.INTEGER, field: 'id_participante', primaryKey: true, autoIncrement: true },
        nome                 : { type: DataTypes.STRING,  field: 'no_participante' },
        documento            : { type: DataTypes.STRING,  field: 'nu_documento' },
        email                : { type: DataTypes.STRING },
        ano_nascimento       : { type: DataTypes.STRING },
        protocolo            : { type: DataTypes.STRING,  field: 'participante_protocolo' },
        sexo_id              : { type: DataTypes.INTEGER, field: 'id_sexo' },
        tipo_escolaridade_id : { type: DataTypes.INTEGER, field: 'id_tipo_escolaridade' },
        tipo_participante_id : { type: DataTypes.INTEGER, field: 'id_tipo_participante' }
    }, {
        tableName: 'td_participante',
        timestamps: false
    });

    return Participante;
};