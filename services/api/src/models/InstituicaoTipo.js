module.exports = (sequelize, DataTypes) => {
    var InstituicaoTipo = sequelize.define('InstituicaoTipo', {
        id     : { type: DataTypes.INTEGER, primaryKey: true, field: 'id_tipo_unidade' },
        nome   : { type: DataTypes.STRING, field: 'ds_tipo_unidade' }
    }, {
        tableName  : 'td_tipo_unidade',
        timestamps : false
    });

    InstituicaoTipo.associate = models => {
        models.InstituicaoTipo.hasMany(models.Instituicao, {foreignKey: 'id_tipo_unidade'});
    };

    return InstituicaoTipo;
};