module.exports = (sequelize, DataTypes) => {
    var Nota = sequelize.define('Nota', {
        id               : { type: DataTypes.INTEGER, field: 'id_nota', primaryKey: true, autoIncrement: true },
        instituicao_id   : { type: DataTypes.STRING, field: 'id_unidade' },
        etapa_id         : { type: DataTypes.INTEGER, field: 'id_etapa' },
        questao_id       : { type: DataTypes.STRING,  field: 'id_questao_constructo' },
        participante_id  : { type: DataTypes.INTEGER, field: 'id_participante' },
        nota_expectativa : { type: DataTypes.INTEGER, field: 'nu_nota_profissional_expectativa' },
        nota_satisfacao  : { type: DataTypes.INTEGER, field: 'nu_nota_usuario_percebida' },
        data             : { type: DataTypes.DATE,    field: 'dt_pesquisa', defaultValue: DataTypes.NOW },
    }, {
        tableName: 'tf_notas',
        timestamps: false
    });

    Nota.associate = models => {
        models.Nota.belongsToMany(models.Profissional, {
            through    : models.ProfissionalNota,
            foreignKey : 'id_nota',
            otherKey   : 'id_profissional'
        });
    };

    return Nota;
};