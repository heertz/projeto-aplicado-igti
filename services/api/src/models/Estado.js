module.exports = (sequelize, DataTypes) => {
    var Estado = sequelize.define('Estado', {
        id  : { type: DataTypes.INTEGER, primaryKey: true, field: 'id_uf' },
        nome: { type: DataTypes.STRING, field: 'no_uf_completo' }
    }, {
        tableName: 'tb_uf',
        timestamps: false
    });

    Estado.associate = models => {
        models.Estado.hasMany(models.Municipio, {foreignKey: 'id_uf'});
    };

    return Estado;
};