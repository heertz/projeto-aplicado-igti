module.exports = (sequelize, DataTypes) => {
    var Pergunta = sequelize.define('Pergunta', {
        id      : { type: DataTypes.INTEGER, primaryKey: true, field: 'id' },
        pergunta: { type: DataTypes.STRING, field: 'ds_pergunta' },
        ordem   : { type: DataTypes.INTEGER, field: 'nu_ordem' }
    }, {
        tableName: 'td_perguntas',
        timestamps: false
    });

    return Pergunta;
};