module.exports = (sequelize, DataTypes) => {
    var Profissional = sequelize.define('Profissional', {
        id     : { type: DataTypes.INTEGER, primaryKey: true, field: 'id_profissional' },
        nome   : { type: DataTypes.STRING, field: 'no_profissional' }
    }, {
        tableName  : 'td_profissional',
        timestamps : false,
        name       : { plural: 'Profissionais' }
    });

    Profissional.associate = models => {
        models.Profissional.belongsTo(models.Instituicao, {foreignKey: 'id_unidade'});
        
        models.Profissional.belongsToMany(models.Nota, {
            through    : models.ProfissionalNota,
            foreignKey : 'id_profissional',
            otherKey   : 'id_nota'
        });
    };

    return Profissional;
};