module.exports = (sequelize, DataTypes) => {
    var Instituicao = sequelize.define('Instituicao', {
        id     : { type: DataTypes.INTEGER, primaryKey: true, field: 'id_unidade' },
        nome   : { type: DataTypes.STRING, field: 'no_fantasia' },
        bairro : { type: DataTypes.STRING, field: 'no_bairro' }
    }, {
        tableName  : 'td_instituicao',
        timestamps : false,
        name       : { plural: 'Instituicoes' }
    });

    Instituicao.associate = models => {
        models.Instituicao.belongsTo(models.InstituicaoTipo, {foreignKey: 'id_tipo_unidade'});
        models.Instituicao.belongsTo(models.Municipio, {foreignKey: 'id_municipio'});

        models.Instituicao.hasMany(models.Profissional, {foreignKey: 'id_unidade'});
    };

    return Instituicao;
};