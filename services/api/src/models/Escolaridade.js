module.exports = (sequelize, DataTypes) => {
    var Escolaridade = sequelize.define('Escolaridade', {
        id_tipo_escolaridade  : { type: DataTypes.INTEGER, primaryKey: true },
        ds_tipo_escolaridade: DataTypes.STRING
    }, {
        tableName: 'td_tipo_escolaridade',
        timestamps: false
    });

    return Escolaridade;
};