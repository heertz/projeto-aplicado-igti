module.exports = (sequelize, DataTypes) => {
    var ProfissionalNota = sequelize.define('ProfissionalNota', {
        profissional_id : { type: DataTypes.INTEGER, field: 'id_profissional', primaryKey: true },
        nota_id         : { type: DataTypes.INTEGER, field: 'id_nota', primaryKey: true }
    }, {
        tableName: 'rl_profissional_nota',
        timestamps: false
    });

    return ProfissionalNota;
};