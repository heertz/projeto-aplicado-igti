let express = require('express'),
    router = express.Router();

router.get('/', (req, res) => res.send('API Web Server'));

router.use('/dados-basicos', require('./controllers/DadosBasicos.controller'));
router.use('/estado(s)?', require('./controllers/Estados.controller'));
router.use('/municipio(s)?', require('./controllers/Municipios.controller'));
router.use('/instituic(ao)?(oes)?', require('./controllers/Instituicoes.controller'));
router.use('/perguntas', require('./controllers/Perguntas.controller'));
router.use('/respostas', require('./controllers/Respostas.controller'));

module.exports = router